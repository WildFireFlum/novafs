import os
import sys

def write_file(file_path, byte_count):
    with open(file_path, 'wb') as myfile:
        for i in range(byte_count):
            cur_byte = (chr(ord('A') + i % 26)).encode()
            myfile.write(cur_byte)
            myfile.flush()
            with open("/proc/fs/NOVA/pmem0/create_snapshot", 'w') as snap_file:
                snap_file.write('1')


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("usage: {} file_path byte_count".format(sys.argv[0]))
        exit(1)

    file_path = sys.argv[1]
    byte_count = int(sys.argv[2])
    write_file(file_path, byte_count)
