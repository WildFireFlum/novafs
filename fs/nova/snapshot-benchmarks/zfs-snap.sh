#sudo apt install zfs-dkms
#sudo modprobe zfs
sudo umount /mnt/ramdisk
sudo zfs umount /mnt/ramdisk
sudo zpool destroy mypool
rm -rf /mnt/ramdisk
sudo zpool create mypool /dev/pmem0 -m /mnt/ramdisk -f
sudo chown ynon /mnt/ramdisk/ -R 
echo timing file writes 
time python random-dirtree.py /mnt/ramdisk/ 100000000 0 1 1
echo created file count
ls /mnt/ramdisk | wc -l
sudo zfs list
echo timing snapshot creation
time sudo zfs snapshot mypool@snap
echo timing snapshot deletion
time sudo zfs destroy mypool@snap
echo waiting
sleep 60
echo timing big umount
time sudo zfs umount mypool
echo timing big mount
time sudo zfs mount mypool
