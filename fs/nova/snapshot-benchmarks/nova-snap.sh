echo timing nova
sudo umount /mnt/ramdisk
echo timing first mount
time sudo mount -t NOVA -o init /dev/pmem0 /mnt/ramdisk/
sudo chown ynon /mnt/ramdisk/ -R
echo timing file writes 
time python random-dirtree.py /mnt/ramdisk/ 100000000 0 1 1
echo created file count
ls /mnt/ramdisk | wc -l
sudo chmod 777 /proc/fs/NOVA/pmem0/create_snapshot
sudo chmod 777 /proc/fs/NOVA/pmem0/delete_snapshot
echo timing snapshot creation
time sudo echo 1 > /proc/fs/NOVA/pmem0/create_snapshot
echo timing snapshot deletion
time sudo echo 0 > /proc/fs/NOVA/pmem0/delete_snapshot
echo waiting
sleep 60
echo timing big umount
time sudo umount /mnt/ramdisk
echo timing big mount
time sudo mount -t NOVA /dev/pmem0 /mnt/ramdisk/