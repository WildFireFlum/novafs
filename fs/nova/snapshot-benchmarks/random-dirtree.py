#! /bin/python3
import sys
import os
import random
import string
from pathlib import Path
from check_logs_on_append import append_file

random.seed(13371337)

NUM_CHARS = 2 ** 12
MAX_SIZE = (2 ** 30) * 3 #100Gbit of chars == 100GB
REAPPEND_COUNT = 1
def random_string(min_length=5, max_length=10):
    """
    Get a random string
    Args:
        min_length: Minimal length of string
        max_length: Maximal length of string
    Returns:
        Random string of ascii characters
    """
    length = random.randint(min_length, max_length)
    return ''.join(
        random.choice(string.ascii_uppercase + string.digits)
        for _ in xrange(length)
    )


def create_random_tree(basedir, nfiles=2, nfolders=1, repeat=1,
                       maxdepth=None, sigma_folders=1, sigma_files=1):
    """
    Create a random set of files and folders by repeatedly walking through the
    current tree and creating random files or subfolders (the number of files
    and folders created is chosen from a Gaussian distribution).
    Args:
        basedir: Directory to create files and folders in
        nfiles: Average number of files to create
        nfolders: Average number of folders to create
        repeat: Walk this often through the directory tree to create new
            subdirectories and files
        maxdepth: Maximum depth to descend into current file tree. If None,
            infinity.
        sigma_folders: Spread of number of folders
        sigma_files: Spread of number of files
    Returns:
       (List of dirs, List of files), all as pathlib.Path objects.
    """
    alldirs = []
    allfiles = []
    size_count = 0
    for i in xrange(repeat):
        for root, dirs, files in os.walk(str(basedir)):
            for _ in xrange(int(random.gauss(nfolders, sigma_folders))):
                p = Path(root) / random_string()
                if not p.exists():
                    p.mkdir()
                alldirs.append(p)
            for _ in xrange(int(random.gauss(nfiles, sigma_files))):
                if size_count == MAX_SIZE:
                    alldirs = list(set(alldirs))
                    allfiles = list(set(allfiles))
                    return alldirs, allfiles
                p = Path(root) / random_string()
                append_file(str(p), REAPPEND_COUNT, NUM_CHARS)

                size_count += NUM_CHARS
                #if size_count % (NUM_CHARS * (1 << 15)) == 0:
                #    print('Written {} files'.format(size_count / NUM_CHARS))
                allfiles.append(p)
            depth = os.path.relpath(root, str(basedir)).count(os.sep)
            if maxdepth and depth >= maxdepth - 1:
                del dirs[:]

    alldirs = list(set(alldirs))
    allfiles = list(set(allfiles))
    return alldirs, allfiles


if __name__ == "__main__":
    from sys import argv
    if len(argv) != 6:
        print(len(argv))
        print(argv)
        print("basedir: {}, nfiles: {}, nfolders: {}, repeat: {}, maxdepth: {}".format(*argv[1:6]))
        exit(1)
    basedir = sys.argv[1]
    nfiles = int(sys.argv[2])
    nfolders = int(sys.argv[3])
    repeat = int(sys.argv[4])
    maxdepth = int(sys.argv[5])
    create_random_tree(basedir=basedir, nfiles=nfiles, nfolders=nfolders, repeat=repeat, maxdepth=maxdepth, sigma_folders=1, sigma_files=1)
