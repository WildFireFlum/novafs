#!/usr/bin/python
from __future__ import print_function
import sys

def reappend_file(path, iterations, write_size):
    for i in range(iterations):
        mode = 'a+' if i == 0 else 'r+b'
        bytes_to_write = int(write_size)
        with open(path, mode) as myfile:
            offset = i * bytes_to_write
            #offset = offset - 1 if offset else offset
            myfile.seek(offset)
            myfile.write('A' * (bytes_to_write + 1))

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('arg_0: iter_count, arg_1: iter_write_size')
        exit(1)

    reappend_file('reopened_file.txt', int(sys.argv[1]), int(sys.argv[2]))
