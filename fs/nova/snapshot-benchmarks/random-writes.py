import sys
import random


with open(sys.argv[1], 'wb') as myfile:
    for _ in xrange(10**9):
        myfile.seek(random.randrange(0, (2**31) - 1))
        myfile.write('A')
