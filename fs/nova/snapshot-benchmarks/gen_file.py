#!/usr/bin/python
import sys


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print 'need one arg for length'
        exit(1)
    with open('gen_py.txt', 'a+') as myfile:
        myfile.write('A' * int(sys.argv[1]))
