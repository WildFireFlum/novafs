echo timing btrfs
sudo umount /mnt/ramdisk
sudo mkfs.btrfs /dev/pmem0 -f
sudo mount /dev/pmem0 /mnt/ramdisk/ -o dax
#sudo btrfs quota enable /mnt/ramdisk/
#sudo btrfs qgroup limit 10G /mnt/ramdisk/
#sudo btrfs qgroup show -reF /mnt/ramdisk/
sudo chown ynon /mnt/ramdisk/ -R
echo timing file writes 
time python random-dirtree.py /mnt/ramdisk/ 100000000 0 1 1
echo created file count
ls /mnt/ramdisk | wc -l
sudo btrfs fi usage /mnt/ramdisk
echo timing snapshot creation
time sudo btrfs subvolume snapshot /mnt/ramdisk/ /mnt/ramdisk/snap
echo timing snapshot deletion
time sudo btrfs subvolume delete -c /mnt/ramdisk/snap
echo timing big umount
time sudo umount /mnt/ramdisk
echo timing big mount
time sudo mount /dev/pmem0 /mnt/ramdisk/
