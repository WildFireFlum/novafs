#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <libexplain/mmap.h>
#include <errno.h>
#include <memory.h>

int main() {
    const int size = 4096;
    printf("before open\n");

    const char* file_path = "/mnt/ramdisk/dax";
    int file = creat(file_path, S_IRWXU | S_IRWXG | S_IRWXO);
    printf("creat result: %d\n", file);
    char buf[size];
    memset(buf, '.', size);
    write(file, buf, size);
    close(file);

    file = open(file_path, O_RDWR| O_ASYNC | O_FSYNC);
    printf("open result: %d\n", file);

    char* dax_addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, file, 0);
    if (dax_addr == MAP_FAILED) {
        printf("mmap failed: %s\n", explain_mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, file, 0));
    }

    dax_addr[0] = 'H';
    dax_addr[1] = 'E';
    dax_addr[2] = 'L';
    dax_addr[3] = 'L';
    dax_addr[4] = 'O';

    printf("before munmap\n");

    int unmap_result = munmap(dax_addr, size);
    close(file);
    printf("unmap_result: %d\n", unmap_result);

    return 0;
}