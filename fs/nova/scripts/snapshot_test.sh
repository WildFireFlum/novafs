#! /bin/bash
DIR="$(dirname $0)"
NOVA_DIR="/mnt/ramdisk"
TEST_FILE="$NOVA_DIR/testfile"
source "$DIR/mount_nova.sh"
echo hello > $TEST_FILE
sudo su root -c"echo 1 > /proc/fs/NOVA/pmem0/create_snapshot"
echo after snapshot > $TEST_FILE
cat "$TEST_FILE"
sudo umount $NOVA_DIR
sudo mount -t NOVA -o snapshot=0,dbgmask=0x00000010 /dev/pmem0 $NOVA_DIR
cat "$TEST_FILE"
sudo umount $NOVA_DIR
source "$DIR/mount_nova.sh"
cat "$TEST_FILE"
