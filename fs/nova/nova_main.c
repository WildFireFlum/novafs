//
// Created by ynon on 7/26/20.
//

#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/parser.h>
#include <linux/vfs.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/seq_file.h>
#include <linux/mount.h>
#include <uapi/linux/mount.h>
#include <linux/mm.h>
#include <linux/ctype.h>
#include <linux/bitops.h>
#include <linux/magic.h>
#include <linux/exportfs.h>
#include <linux/random.h>
#include <linux/cred.h>
#include <linux/list.h>
#include <linux/dax.h>
#include "nova.h"
#include "journal.h"
#include "super.h"
#include "inode.h"

module_param(measure_timing, int, 0444);
MODULE_PARM_DESC(measure_timing, "Timing measurement");

module_param(metadata_csum, int, 0444);
MODULE_PARM_DESC(metadata_csum, "Protect metadata structures with replication and checksums");

module_param(wprotect, int, 0444);
MODULE_PARM_DESC(wprotect, "Write-protect pmem region and use CR0.WP to allow updates");

module_param(data_csum, int, 0444);
MODULE_PARM_DESC(data_csum, "Detect corruption of data pages using checksum");

module_param(data_parity, int, 0444);
MODULE_PARM_DESC(data_parity, "Protect file data using RAID-5 style parity.");

module_param(dram_struct_csum, int, 0444);
MODULE_PARM_DESC(dram_struct_csum, "Protect key DRAM data structures with checksums");

module_param(nova_dbgmask, int, 0444);
MODULE_PARM_DESC(nova_dbgmask, "Control debugging output");

extern struct kmem_cache *nova_inode_cachep;
extern struct kmem_cache *nova_range_node_cachep;
extern struct kmem_cache *nova_snapshot_info_cachep;


int nova_fill_super(struct super_block *sb, void *data, int silent);

struct dentry *nova_mount(struct file_system_type *fs_type,
                          int flags, const char *dev_name, void *data)
{
    return mount_bdev(fs_type, flags, dev_name, data, nova_fill_super);
}

struct file_system_type nova_fs_type = {
        .owner		= THIS_MODULE,
        .name		= "NOVA",
        .mount		= nova_mount,
        .kill_sb	= kill_block_super,
};

int __init init_rangenode_cache(void)
{
    nova_range_node_cachep = kmem_cache_create("nova_range_node_cache",
                                               sizeof(struct nova_range_node),
                                               0, (SLAB_RECLAIM_ACCOUNT |
                                                   SLAB_MEM_SPREAD), NULL);
    if (nova_range_node_cachep == NULL)
        return -ENOMEM;
    return 0;
}

void init_once(void *foo)
{
    struct nova_inode_info *vi = foo;

    inode_init_once(&vi->vfs_inode);
}

void destroy_inodecache(void)
{
    /*
     * Make sure all delayed rcu free inodes are flushed before
     * we destroy cache.
     */
    rcu_barrier();
    kmem_cache_destroy(nova_inode_cachep);
}

void destroy_rangenode_cache(void)
{
    kmem_cache_destroy(nova_range_node_cachep);
}

void destroy_snapshot_info_cache(void)
{
    kmem_cache_destroy(nova_snapshot_info_cachep);
}

int __init init_snapshot_info_cache(void)
{
    nova_snapshot_info_cachep = kmem_cache_create(
            "nova_snapshot_info_cache",
            sizeof(struct snapshot_info),
            0, (SLAB_RECLAIM_ACCOUNT |
                SLAB_MEM_SPREAD), NULL);
    if (nova_snapshot_info_cachep == NULL)
        return -ENOMEM;
    return 0;
}

//////// Init functions
int __init init_inodecache(void)
{
    nova_inode_cachep = kmem_cache_create("nova_inode_cache",
                                          sizeof(struct nova_inode_info),
                                          0, (SLAB_RECLAIM_ACCOUNT |
                                              SLAB_MEM_SPREAD), init_once);
    if (nova_inode_cachep == NULL)
        return -ENOMEM;
    return 0;
}


static int __init init_nova_fs(void)
{
    int rc = 0;
    INIT_TIMING(init_time);

    NOVA_START_TIMING(init_t, init_time);

    nova_warn("Hello, World!\n");

    if (arch_has_clwb())
        support_clwb = 1;

    nova_info("Arch new instructions support: CLWB %s\n",
              support_clwb ? "YES" : "NO");

    nova_proc_root = proc_mkdir(proc_dirname, NULL);

    nova_dbg("Data structure size: inode %lu, log_page %lu, file_write_entry %lu, dir_entry(max) %d, setattr_entry %lu, link_change_entry %lu\n",
             sizeof(struct nova_inode),
             sizeof(struct nova_inode_log_page),
             sizeof(struct nova_file_write_entry),
             NOVA_DIR_LOG_REC_LEN(NOVA_NAME_LEN),
             sizeof(struct nova_setattr_logentry),
             sizeof(struct nova_link_change_entry));

    rc = init_rangenode_cache();
    if (rc)
        return rc;

    rc = init_inodecache();
    if (rc)
        goto out1;

    rc = init_snapshot_info_cache();
    if (rc)
        goto out2;

    rc = register_filesystem(&nova_fs_type);
    if (rc)
        goto out3;

    NOVA_END_TIMING(init_t, init_time);
    return 0;

    out3:
    destroy_snapshot_info_cache();
    out2:
    destroy_inodecache();
    out1:
    destroy_rangenode_cache();
    return rc;
}

static void __exit exit_nova_fs(void)
{
    unregister_filesystem(&nova_fs_type);
    remove_proc_entry(proc_dirname, NULL);
    destroy_snapshot_info_cache();
    destroy_inodecache();
    destroy_rangenode_cache();
}

module_init(init_nova_fs)
module_exit(exit_nova_fs)

