#!/bin/python
import sys

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print 'arg_0: offset, arg_1: data size'
        exit(1)
    with open('gen_py.txt', 'r+b') as myfile:
        myfile.seek(int(sys.argv[1]))
        myfile.write('B' * int(sys.argv[2]))
