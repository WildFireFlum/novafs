import os
import sys


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("usage: {} file_path".format(sys.argv[0]))
        exit(1)

    file_path = sys.argv[1]
    myfile = open(file_path, 'rb')
